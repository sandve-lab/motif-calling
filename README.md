# motif-calling

Snakemake pipeline to call regulatory motif sequences in a genome, given input data like atac-seq peaks from 'peak-calling' pipeline.

This pipepline aims to:  
+ Take in peak-calling data  
+ Return motif annotations  


## Software requirements

+ Snakemake version 5.5.0 : https://snakemake.readthedocs.io/en/stable/getting_started/installation.html  
+ Meme


## Input

+ Peaks


## Usage

Run snakemake file


## Output

+ Motifs


## DAG

