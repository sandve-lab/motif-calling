import pandas as pd
import os

# Input
configfile: "config.yaml"
SAMPLES = pd.read_csv(config["SAMPLE_FILE"], sep = "\t")
if ("name" not in list(SAMPLES.columns)) or ("path" not in list(SAMPLES.columns)):
  sys.exit("Sample file " + config["SAMPLE_FILE"] + " does not contain columns 'name' or 'path', tab separated.")
if SAMPLES.isnull().values.any():
  sys.exit("Sample file " + config["SAMPLE_FILE"] + " contained some empty values")
GENOME = config["GENOME"]
MOTIFS_DIR = config["MOTIFS_DIR"]
MOTIFS = os.listdir(MOTIFS_DIR)
CENTRAL_MOTIF = config["CENTRAL_MOTIF"]
GENE_REGIONS = config["GENE_REGIONS"]

  
# Output
OUT_DIR = config["OUT_DIR"]

rule all:
  input:
    OUT_DIR + "/count_summary.txt"

def get_sample_path(wildcards):
  idx = SAMPLES.index[(SAMPLES["name"] == wildcards.sample)].tolist()
  return(SAMPLES["path"].loc[idx].tolist())
  
rule extract_peaks:
  input:
    get_sample_path
  output:
    OUT_DIR + "/{sample}/peaks.fasta"
  shell:
    """
    module load bedtools
    bedtools getfasta -name -s -fi {GENOME} -bed {input} | sed 's/>/>{wildcards.sample}:/' > {output}
    """

rule markov:
  input:
    OUT_DIR + "/{sample}/peaks.fasta"
  output:
    OUT_DIR + "/{sample}/background.markov"
  params:
    m = 1
  shell:
    """
    module load meme/4.12.0
    fasta-get-markov -m {params.m} < {input} > {output}
    """

rule fimo:
  input:
    fasta = OUT_DIR +"/{sample}/peaks.fasta",
    bg = OUT_DIR + "/{sample}/background.markov",
    motif = MOTIFS_DIR + "/{motif}"
  output:
    temp(OUT_DIR + "/{sample}/motif_hits/{motif}.fimo")
  shell:
    """
    module load meme/4.12.0
    fimo --verbosity 1 --text --bgfile {input.bg} {input.motif} {input.fasta} > {output}
    """

rule join_fimo_results:
  input:
    expand(OUT_DIR + "/{sample}/motif_hits/{motif}.fimo",
           sample = "{sample}",
           motif = MOTIFS)
  output:
    OUT_DIR + "/{sample}/fimo_results.txt"
  shell:
    "cat {input} | sed 's/^#\s*//' > {output}"
    
rule process_fimo_results:
  input:
    fimo = OUT_DIR + "/{sample}/fimo_results.txt",
    central_motif = CENTRAL_MOTIF,
    gene_regions = GENE_REGIONS
  output:
    joined = OUT_DIR + "/{sample}/fimo_results.joined.txt",
    ranked = OUT_DIR + "/{sample}/fimo_results.ranked.txt",
    gene_overlap = OUT_DIR + "/{sample}/fimo_results.gene_overlap.txt",
    counts = OUT_DIR + "/{sample}/fimo_results.counts.txt"
  script:
    "process_fimo.R"

rule summarise_counts:
  input:
    expand(OUT_DIR + "/{sample}/fimo_results.counts.txt",
           sample = SAMPLES["name"])
  output:
    OUT_DIR + "/count_summary.txt"
  script:
      "count_summary.R"
